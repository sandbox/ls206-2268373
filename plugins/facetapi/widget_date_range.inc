<?php

/**
 * @file
 * Date range widget
 */

/**
 * Slider widget for range filtering.
 */
class FacetapiDateRange extends FacetapiWidget {

  /**
   * Renders the form.
   */
  public function execute() {

    $facet = $this->facet->getFacet();
    $adapter = $this->build['#adapter'];

    // Get the dates
    $build = $this->facet->getBuild();

    // Get a list of all available dates
    $dates = $this->buildDateList($build);
    // Sort the dates asc
    sort($dates);
    // Get the min and max values
    $min_date = reset($dates);
    $max_date = end($dates);
    // If there is only one date, assume a selection has been made.
    if (count($build) === 1) {
      $keys = array_keys($build);
      $date = reset($keys);
      // get the min and max values to use as placeholders
      $this->build['#min_placeholder'] = $this->getMinDate($date);
      $this->build['#max_placeholder'] = $this->getMaxDate($date);
    }
    // Store the data to use in the form callback
    $this->build['#dates'] = $dates;
    $this->build['#min_date'] = $min_date;
    $this->build['#max_date'] = $max_date;

    // Render form to filter dates
    $this->build[$this->facet['field alias']] = drupal_get_form(
      'facetapi_date_range_widget_form_' . $this->facet['field alias'],
      $adapter, $this->build
    );
  }

  /**
   * Recursive function to build out list of dates
   *
   * @param $dates
   * @param array $date_test
   * @return array
   */
  private function buildDateList($dates, &$date_test = array()) {
    foreach ($dates as $date => $other) {
      if (!empty($other['#item_children'])) {
        $this->buildDateList($other['#item_children'], $date_test);
      }
      else {
        $date_test[] = $this->getMinDate($date);
        $date_test[] = $this->getMaxDate($date);
      }
    }
    return $date_test;
  }

  /**
   * Form builder function for the facet block.
   */
  public static function widgetForm($form, $form_state, $form_id, FacetapiAdapter $adapter, $build = array()) {
    // Get defaults
    $min_date = (isset($build['#min_date'])) ? $build['#min_date'] : 0;
    $max_date = (isset($build['#max_date'])) ? $build['#max_date'] : 0;

    // @todo: convert these hidden fields to js settings
    $form['date_range']['min_value'] = array(
      '#type' => 'hidden',
      '#value' => $min_date,
      '#attributes' => array('id' => 'date_range_min_value'),
    );
    $form['date_range']['max_value'] = array(
      '#type' => 'hidden',
      '#value' => $max_date,
      '#attributes' => array('id' => 'date_range_max_value'),
    );

    // Display fields for the date range
    // We submit dates in another format so the display is separate
    $form['date_range']['min_display'] = array(
      '#type' => 'textfield',
      '#title' => t('Start date'),
      '#size' => 15,
      '#attributes' => array(
        'class' => array('facetapi-date_range-min'),
        //'placeholder' => (isset($build['#min_placeholder'])) ? $build['#min_placeholder'] : 'Start date'
      ),
    );

    $form['date_range']['max_display'] = array(
      '#type' => 'textfield',
      '#title' => t('End date'),
      '#size' => 15,
      '#attributes' => array(
        'class' => array('facetapi-date_range-max'),
        //'placeholder' => (isset($build['#max_placeholder'])) ? $build['#max_placeholder'] : 'End date'
      ),
    );

    // Actual data to submit
    $form['date_range']['min'] = array(
      '#type' => 'hidden',
      '#title' => t('Start date alt'),
      '#size' => 15,
      '#default_value' => ($min_date) ? $min_date . 'T00:00:00Z' : 0,
      '#attributes' => array(
        'id' => 'facetapi-date_range-min_alt',
      ),
    );

    $form['date_range']['max'] = array(
      '#type' => 'hidden',
      '#title' => t('End date alt'),
      '#size' => 15,
      '#default_value' => ($max_date) ? $max_date . 'T00:00:00Z' : 0,
      '#attributes' => array(
        'id' => 'facetapi-date_range-max_alt',
      ),
    );
    $form['date_range']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    $exclude = array('q' => 1, 'page' => 1);
    if (isset($build['#facet']['field alias'])) {
      $exclude[$build['#facet']['field alias']] = 1;
    }
    $form['date_range']['field_alias'] = array(
      '#type' => 'hidden',
      '#default_value' => isset($build['#facet']['field alias']) ? $build['#facet']['field alias'] : '',
    );
    $form['date_range']['field_type'] = array(
      '#type' => 'hidden',
      '#default_value' => isset($build['#facet']['field type']) ? $build['#facet']['field type'] : '',
    );
    $form['date_range']['get'] = array(
      '#type' => 'hidden',
      '#default_value' => json_encode(array_diff_key($_GET, $exclude)),
    );
    $form['date_range']['path'] = array(
      '#type' => 'hidden',
      '#default_value' => $_GET['q'],
    );
    // Only add JS if we are on the actual search page.
    if (isset($build['#attributes'])) {
      $form['#attributes'] = $build['#attributes'];
      $form['date_range']['#attached']['css'][] = drupal_get_path('module', 'facetapi_date_range') . '/css/facetapi_date_range.css';
      $form['date_range']['#attached']['js'][] = drupal_get_path('module', 'facetapi_date_range') . '/js/jquery-ui-1.10.4.datepicker.min.js';
      $form['date_range']['#attached']['js'][] = drupal_get_path('module', 'facetapi_date_range') . '/js/facetapi_date_range.js';
    }
    $form['#action'] = url('facetapi_date_range/widget/submit/' . $form_id . '/' . $adapter->getSearcher());
    return $form;
  }

  public static function widgetFormValidate($form, &$form_state) {
    //Nothing to do...
  }

  public static function widgetFormSubmit($form, &$form_state) {
    $adapter = $form['#facetapi_adapter'];
    $url_processor = $adapter->getUrlProcessor();
    $filter_key = $url_processor->getFilterKey();

    $query_string = (array) json_decode($form_state['values']['get'], TRUE);
    if (empty($query_string[$filter_key])) {
      $query_string[$filter_key] = array();
    }

    $my_field = rawurlencode($form_state['values']['field_alias']);

    // Remove any existing filters for my field.
    foreach ($query_string[$filter_key] as $idx => $filter) {
      $parts = explode(':', $filter, 2);
      if ($parts[0] == $my_field) {
        unset($query_string[$filter_key][$idx]);
      }
    }

    $query_string[$filter_key][] = $my_field . ':' . "[{$form_state['values']['min']} TO {$form_state['values']['max']}]";
    $form_state['redirect'] = array($form_state['values']['path'], array('query' => $query_string));
  }

  /**
   * Returns defaults for the settings this widget provides.
   */
  function getDefaultSettings() {
    return array(
      'prefix' => '',
      'suffix' => '',
      'precision' => '0',
      'step' => '1',
    );
  }

  /**
   * Get the min date from solr date format
   *
   * @param $date
   * @param string $format
   * @return bool|string
   */
  private function getMinDate($date, $format = 'Y-m-d') {
    preg_match_all('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $date, $matches);
    $min_date = reset($matches[0]);
    return date($format, strtotime($min_date));
  }

  /**
   * Get the max date from solr date format
   *
   * @param $date
   * @param string $format
   * @return bool|string
   */
  private function getMaxDate($date, $format = 'Y-m-d') {
    preg_match_all('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $date, $matches);
    $min_date = end($matches[0]);
    return date($format, strtotime($min_date));
  }

}
