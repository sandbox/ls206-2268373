(function($) {
  $(function() {
    var min, max;
    min = new Date($('#date_range_min_value').val());
    max = new Date($('#date_range_max_value').val());
    $(".facetapi-date_range-min").datepicker({
      dateFormat: 'dd/mm/yy',
      defaultDate: "+1w",
      changeMonth: true,
      changeYear: true,
      numberOfMonths: 1,
      minDate: min,
      maxDate: max,
      altField: "#facetapi-date_range-min_alt",
      altFormat: "yy-mm-ddT00:00:00Z",
      onSelect: function(selectedDate) {
        $(".facetapi-date_range-max").datepicker("option", "minDate", selectedDate );
      }
    });
    $(".facetapi-date_range-max").datepicker({
      dateFormat: 'dd/mm/yy',
      defaultDate: "+1w",
      changeMonth: true,
      changeYear: true,
      numberOfMonths: 1,
      minDate: min,
      maxDate: max,
      altField: "#facetapi-date_range-max_alt",
      altFormat: "yy-mm-ddT00:00:00Z",
      onSelect: function(selectedDate) {
        $(".facetapi-date_range-min").datepicker("option", "maxDate", selectedDate);
      }
    });
  });
})(jQuery);